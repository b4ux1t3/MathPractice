﻿using System.Diagnostics;
using System.IO.Pipes;
using MathPractice.Data;
using MathPractice.Generation;
using McMaster.Extensions.CommandLineUtils;

namespace MathPractice.Cli;

public class Program
{
    public static int Main(string[] args) => CommandLineApplication.Execute<Program>(args);

    [Option(template: "-n|--count <N>", Description = "The number correct before the test ends.")] private int NumCorrect { get; } = 20;
    [Option(template: "--max-add", Description = "The mux number in addition problems")] private int MaxAdd { get; } = 99;
    [Option(template: "--max-sub", Description = "The mux number in subtraction problems")] private int MaxSub { get; } = 99;
    [Option(template: "--max-mul", Description = "The mux number in multiplication problems")] private int MaxMul { get; } = 99;
    [Option(template: "--max-div", Description = "The mux number in division problems")] private int MaxDiv { get; } = 99;
    [Option(template: "-na", Description = "Disable addition problems")] private bool DisableAddition { get; } = false;
    [Option(template: "-ns", Description = "Disable subtraction problems")] private bool DisableSubtraction { get; } = false;
    [Option(template: "-nm", Description = "Disable multiplication problems")] private bool DisableMultiplication { get; } = false;
    [Option(template: "-nd", Description = "Disable division problems")] private bool DisableDivision { get; } = false;
    [Option(template: "-e", Description = "Solve endless problems forever. q to exit")] private bool Endless { get; } = false;

    private bool _exit = false;
    
    private void OnExecute()
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        if (DisableAddition && DisableSubtraction && DisableMultiplication && DisableDivision)
        {
            Console.WriteLine("Well, no problems, so I guess you're done!");
            return;
        }
        var generator = new ProblemGenerator(
            maxAdd: MaxAdd, 
            maxSub: MaxSub, 
            maxMul: MaxMul, 
            maxDiv: MaxDiv,
            disableAdd: DisableAddition,
            disableSub: DisableSubtraction,
            disableMul: DisableMultiplication,
            disableDiv: DisableDivision
        );
        var currentProblem = generator.NewProblem();
        var answers = new List<(MathProblem, int, bool)>();

        var sw = Stopwatch.StartNew();
        
        while (!_exit)
        {
            Console.Clear();
            Console.Write(currentProblem.ToString());
            var input = Console.ReadLine();
            if (input is null || !int.TryParse(input, out var answer))
            {
                _exit = Endless && input == "q";
                continue;
            }
            answers.Add((currentProblem, answer, answer == currentProblem.Answer));
            currentProblem = generator.NewProblem();
            _exit = Endless ? !Endless : answers.Count(a => a.Item3) >= NumCorrect;
        }

        var time = sw.Elapsed;
        Console.Clear();

        foreach (var answer in answers)
        {
            var emoji = answer.Item3 ? "✔" : $"❌ ({answer.Item1.Answer})";
            Console.WriteLine($"{answer.Item1}{answer.Item2} {emoji}");
        }
        if (Endless)
        {
            Console.WriteLine($"Total: {answers.Count(a => a.Item3)}/{answers.Count}");
            return;
        }
        Console.WriteLine($"{NumCorrect} correct answers in {time:g}");
        Console.WriteLine($"Total: {NumCorrect}/{answers.Count}");
    }
}