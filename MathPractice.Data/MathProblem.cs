﻿namespace MathPractice.Data;

public enum Operation
{
    Add,
    Subtract,
    Multiply,
    Divide
}

public record MathProblem
{
    public int OperandA { get; }
    public int OperandB { get; }
    public Operation Operation { get; }
    private static readonly Func<int, int, int> Add = (a, b) => a + b;
    private static readonly Func<int, int, int> Sub = (a, b) => a - b;
    private static readonly Func<int, int, int> Mul = (a, b) => a * b;
    private static readonly Func<int, int, int> Div = (a, b) => a / b;

    public MathProblem(int operandA, int operandB, Operation operation)
    {
        if (operandB == 0 && operation == Operation.Divide) throw new DivideByZeroException($"{nameof(MathProblem)} given a zero as a divisor.");
        OperandA = operandA;
        OperandB = operandB;
        Operation = operation;
    }
    
    private static readonly Dictionary<Operation, Func<int, int, int>> OperationLookup = new()
    {
        {Operation.Add, Add},
        {Operation.Subtract, Sub},
        {Operation.Multiply, Mul},
        {Operation.Divide, Div},
    };
    
    private static readonly Dictionary<Operation, char> OperationCharacterLookup = new()
    {
        {Operation.Add, '+'},
        {Operation.Subtract, '-'},
        {Operation.Multiply, '×'},
        {Operation.Divide, '÷'},
    };
    public int Answer => OperationLookup[Operation](OperandA, OperandB);
    private char Operator => OperationCharacterLookup[Operation];
    public override string ToString() => $"{OperandA} {Operator} {OperandB} = ";
}