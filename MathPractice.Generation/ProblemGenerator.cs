﻿using MathPractice.Data;

namespace MathPractice.Generation;

public class ProblemGenerator
{
    private readonly int _maxAdd;
    private readonly int _maxSub;
    private readonly int _maxMul;
    private readonly int _maxDiv;
    
    private static readonly Random Rand = new();

    private readonly List<Operation> _operationsAvailable = new();

    public ProblemGenerator(int maxAdd = 99, int maxSub = 99, int maxMul = 99, int maxDiv = 99, bool disableAdd = false, bool disableSub = false, bool disableMul = false, bool disableDiv = false)
    {
        _maxAdd = maxAdd;
        _maxSub = maxSub;
        _maxMul = maxMul;
        _maxDiv = maxDiv;
        
        if (!disableAdd) _operationsAvailable.Add(Operation.Add);
        if (!disableSub) _operationsAvailable.Add(Operation.Subtract);
        if (!disableMul) _operationsAvailable.Add(Operation.Multiply);
        if (!disableDiv) _operationsAvailable.Add(Operation.Divide);
    }
    public MathProblem NewProblem()
    {
        var operation = _getOp();
        var opA = Rand.Next(GetMax(operation));
        var opB = Rand.Next(GetMax(operation));

        while (operation is Operation.Divide && (opB == 0 || (opB == opA && Rand.Next(2) == 1) || opA < opB || opA % opB != 0))
        {
            opA = Rand.Next(GetMax(operation));
            opB = Rand.Next(GetMax(operation));
        }
        
        return new(opA, opB, operation);
    }

    private int GetMax(Operation op) => op switch
    {
        Operation.Add => _maxAdd,
        Operation.Subtract => _maxSub,
        Operation.Multiply => _maxMul,
        Operation.Divide => _maxDiv,
        _ => throw new ArgumentOutOfRangeException(nameof(op), op, null)
    };

    private Operation _getOp()
    {
        var operation = _operationsAvailable[Rand.Next(_operationsAvailable.Count)];

        return operation;
    }
}